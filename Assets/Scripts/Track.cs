﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class Track : MonoBehaviour
{
    public List<Waypoint> Waypoints;
    public Profile Profile;
    public float NewWPDistance = 4;
    public bool isClosed;
    private Mesh mesh;
    private Vector3[] vertices;
    private int[] triangles;
    private float[] profileLengths01;

    private void OnValidate()
    {
        if (!Application.isPlaying)
        {
            CreateMesh();
        }
        
        
    }
    private void OnEnable()
    {
        CreateMesh();
    }
    private void Awake()
    {
        if(mesh == null)
        {
           var mf = GetComponent<MeshFilter>();
           mesh = new Mesh();
           mf.mesh = mesh;
           mesh.name = "Track Mesh";
        }
    }

    public void AddWaypoint()
    {
        if(Waypoints == null) { return; }
        var wp = new Waypoint();

        switch (Waypoints.Count)
        {
            case 0:
                wp.Position = Vector3.zero;
                break;

            case 1:
                wp.Position = Waypoints[0].Position + Vector3.forward * NewWPDistance;
                break;

            default:
                Vector3 lastPos = Waypoints[Waypoints.Count - 1].Position;
                Vector3 secondToLastPos = Waypoints[Waypoints.Count - 2].Position;
                Vector3 dir = (lastPos - secondToLastPos).normalized;
                wp.Position = lastPos + dir * NewWPDistance;
                break;
        }
        wp.Rotation = Quaternion.identity;
        Waypoints.Add(wp);
    }

    public void RemoveLastWayPoint()
    {
        if(Waypoints.Count < 1) { return; }
        Waypoints.RemoveAt(Waypoints.Count - 1);
    }
    public void CreateMesh()
    {
        if (mesh == null) { return; }
            mesh.Clear();
        SetVertices();
        SetTriangles();
        Calculated();
        SetUVs();
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        GetComponent<MeshCollider>().sharedMesh = mesh;
    }

    private void SetVertices()
    {
        int noOfVerts = Waypoints.Count * Profile.Points.Count;
        if (vertices == null || vertices.Length != noOfVerts)
        {
            vertices = new Vector3[noOfVerts];
        }
        for (int w = 0, v = 0; w < Waypoints.Count; w++)
        {
            Vector3 wpPos = Waypoints[w].Position;
            Quaternion wpRot = Waypoints[w].Rotation;
            for (int p = 0; p < Profile.Points.Count; p++)
            {
                vertices[v] = wpPos + (wpRot * Profile.Points[p]);
                v++;
            }
        }
        mesh.vertices = vertices;
        if (mesh == null) { return; }
    }

    private void SetTriangles()
    {
        int noOftris = !isClosed ?
           Mathf.Clamp(6 * (Profile.Points.Count - 1) * (Waypoints.Count - 1), 0, int.MaxValue) : 
           Mathf.Clamp (6 * (Profile.Points.Count - 1) * Waypoints.Count, 0 , int.MaxValue);

        if ( triangles == null || triangles.Length != noOftris)
        {
            triangles = new int[noOftris];
        }

        int ppc = Profile.Points.Count;
        int wpc = Waypoints.Count;
        int maxW = isClosed ? wpc : wpc - 1;


        for (int w = 0, t = 0; w < maxW; w++ )
        {
            for (int p = 0; p < ppc - 1; p++ , t += 6)
            {
                int wNext = (w + 1) % wpc;
                int ll= (w * ppc) + p;
                int lr = ll + 1;
                int ul = (wNext * ppc) + p;
                int ur = ul + 1;
                triangles[t] = ll;
                triangles[t + 1] = triangles[t + 3] = ul;
                triangles[t + 2] = triangles[t + 5] = lr;
                triangles[t + 4] = ur;
               
            }
        }
        mesh.triangles = triangles;
        if (mesh == null) { return; }
    }

    private void SetUVs()
    {
        var uvs = new Vector2[vertices.Length];
        for (int w = 0; w < Waypoints.Count; w++)
        {
            for (int p = 0; p < Profile.Points.Count; p++)
            {
                //float u = (float)p / (Profile.Points.Count - 1);
                float u = p > 0? profileLengths01[p - 1] : 0;
                float v = w;
                int idx = w * Profile.Points.Count + p;
                uvs[idx] = new Vector2(u, v);
            }
           
        }
        //var uvs = new Vector2[4]
        //{
        //    new Vector2[0,0],
        //    new Vector2[1,0],
        //    new Vector2[0,1],
        //    new Vector2[1,1],
        //};
        mesh.uv = uvs;
    }


    public void AddProfilePoint()
    {
        if(Profile.Points==null) { return; }
        List<Vector3> points = Profile.Points;
        Vector3 newPoint = new Vector3();
        switch (points.Count)
        {
            case 0:
                newPoint = Vector3.zero;
                break;
            case 1:
                newPoint = points[0] + Vector3.right;
                break;

            default:
                Vector3 lastPos = points[points.Count - 1];
                Vector3 secondToLastPos = points[points.Count - 2];
                Vector3 dir = (lastPos - secondToLastPos).normalized;
                newPoint = lastPos + dir;
                break;
        }
        points.Add(newPoint);
        
    }

    private void Calculated()
    {
        profileLengths01 = new float[Profile.Points.Count - 1];
        float pfeffer = 0f;
        for (int i = 0; i < profileLengths01.Length; i++)
        {
            pfeffer += Vector3.Distance(Profile.Points[i], Profile.Points[i + 1]);
            profileLengths01[i] = pfeffer;
        }
        float sum = profileLengths01.Sum();
        for (int j = 0; j < profileLengths01.Length; j++)
        {
            profileLengths01[j] = profileLengths01[j] / pfeffer;
        }
    }
    public void RemoveLastprofilePoint()
    {
        if(Profile.Points.Count < 1) { return; }
        Profile.Points.RemoveAt(Profile.Points.Count - 1);
    }
}



[System.Serializable]
public class Waypoint
{
    public Interpolation Interpolation;
    public Vector3 Position;
    [HideInInspector]
    public Quaternion Rotation;
}

[System.Serializable]
public class Profile
{
    public List<Vector3> Points;
}
public enum Interpolation { Line, Arc}
