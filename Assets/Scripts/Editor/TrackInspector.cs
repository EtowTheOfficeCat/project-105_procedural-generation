﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Track))]
public class TrackInspector : Editor
{
    private enum HandleType { FreeMove, Arrows, Rotation}
    private HandleType handletype = HandleType.FreeMove;
    private enum EditOption { None, Waypoints, Profile}
    private EditOption editOption = EditOption.None;
    private Track track;
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        Track track = target as Track;
        editOption = (EditOption)EditorGUILayout.EnumPopup("Edit Option", editOption);
        SerializedProperty isClosed = serializedObject.FindProperty("isClosed");
        EditorGUILayout.PropertyField(isClosed);

        switch (editOption)
        {
            case EditOption.None:
                break;

            case EditOption.Waypoints:
                handletype = (HandleType)EditorGUILayout.EnumPopup("Handle Type", handletype);

                if (GUILayout.Button("Ad Waypoint"))
                {
                    track.AddWaypoint();
                    track.CreateMesh();
                }
                if (GUILayout.Button("Remove last Waypoint"))
                {
                    track.RemoveLastWayPoint();
                    track.CreateMesh();
                }
                
                SerializedProperty newWPDistance = serializedObject.FindProperty("NewWPDistance");
                EditorGUILayout.PropertyField(newWPDistance);
                SerializedProperty waypoints = serializedObject.FindProperty("Waypoints");
                EditorGUILayout.PropertyField(waypoints, true);
                break;

            case EditOption.Profile:

                if (GUILayout.Button("Add Profile Point"))
                {
                    track.AddProfilePoint();
                    track.CreateMesh();
                }
                if (GUILayout.Button("Remove Last Profile Point"))
                {
                    track.RemoveLastprofilePoint();
                    track.CreateMesh();
                }

                SerializedProperty profile = serializedObject.FindProperty("Profile");
                EditorGUILayout.PropertyField(profile, true);
                break;

            default:
                break;
        }
        serializedObject.ApplyModifiedProperties();
    }

    private void OnSceneGUI()
    {
        Track track = target as Track;
        switch (editOption)
        {
            case EditOption.None:
                
                break;
            case EditOption.Waypoints:

                EditWayPoints(track);
                break;
            case EditOption.Profile:
                EditProfile(track);
                break;
            default:
                break;
        }
    }

    private void EditWayPoints(Track track)
    {
        if(track.Waypoints == null) { return; }
        for (int i = 0; i < track.Waypoints.Count ; i++)
        {
            bool isFirst = i == 0;
            if (!isFirst)
            {
               
                Vector3 pos1 = track.transform.TransformPoint(track.Waypoints[i].Position);
                Vector3 pos2 = track.transform.TransformPoint(track.Waypoints[i-1].Position);
                Handles.color = Color.magenta;
                Handles.DrawDottedLine(pos1, pos2, 4f);
            }
            Handles.color = isFirst ? Color.green : Color.magenta;
            Vector3 oldPos = track.transform.TransformPoint(track.Waypoints[i].Position);
            Vector3 newPos = oldPos;
            Quaternion oldRot = track.Waypoints[i].Rotation;
            Quaternion newRot = oldRot;
            switch (handletype)
            {
                case HandleType.FreeMove:
                    newPos = Handles.FreeMoveHandle(oldPos, Quaternion.identity, 0.5f, Vector3.one, Handles.DotHandleCap);
                    break;
                case HandleType.Arrows:
                    newPos = Handles.PositionHandle(oldPos, oldRot);
                    break;
                case HandleType.Rotation:
                    newRot = Handles.RotationHandle(oldRot, oldPos);
                    break;
                default:
                    break;
            }
            if(newPos != oldPos)
            {
                track.Waypoints[i].Position = track.transform.InverseTransformPoint(newPos);
                track.CreateMesh();
            }
           
            if(newRot != oldRot)
            {
                track.Waypoints[i].Rotation = newRot;
                track.CreateMesh();
            }
        }
    }

    private void EditProfile(Track track)
    {
        List<Vector3> points = track.Profile.Points;
        if (points == null) { return; }
        for (int i = 0; i < points.Count; i++)
        {

            bool isFirst = i == 0;
            if (!isFirst)
            {
                Handles.color = Color.yellow;
                Vector3 pos1 = track.transform.TransformPoint(points[i]);
                Vector3 pos2 = track.transform.TransformPoint(points[i - 1]);
                Handles.DrawLine(pos1, pos2);

            }
            Handles.color = isFirst ? Color.green : Color.blue;
            Vector3 oldPoint = track.transform.TransformPoint(points[i]);
            Vector3 newPoint = Handles.FreeMoveHandle(
                oldPoint,
                Quaternion.identity,
                0.2f,
                Vector3.one * 0.25f,
                Handles.DotHandleCap);
            newPoint = new Vector3(newPoint.x, newPoint.y, 0f);
            if(newPoint != oldPoint)
            {
                points[i] = track.transform.InverseTransformPoint(newPoint);
                track.CreateMesh();
            }
        }
    }

}
