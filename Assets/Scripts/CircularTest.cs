﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularTest : MonoBehaviour
{
    [SerializeField] private float maxAngle = 30;
    [SerializeField] private float radius = 1f;
    [SerializeField] private int noOfPoints = 4;
    [SerializeField] private float height;
    private Vector3 center;
    Vector3 firstPoint;
    Vector3 secondPoint;

    private void Start()
    {
        
    }

    Vector3 getPointOnCircle (float angle)
    {
        float a = angle * Mathf.Deg2Rad;
        firstPoint = center + new Vector3(radius, 0, 0);
    float x = Mathf.Cos(a) * radius;
    float z = Mathf.Sin(a) * radius;
        return new Vector3(x, 0, z);
    }

private void OnDrawGizmos()
    {
        center = transform.position;
        Gizmos.DrawWireSphere(center, 0.1f);
        float step = maxAngle / noOfPoints;
        for (int i = 0; i < noOfPoints; i++)
        {
            Vector3 p = center + getPointOnCircle(step * i);
            p = new Vector3(p.x, (i * height) / noOfPoints, p.z );
            Gizmos.DrawWireSphere(p, 0.1f);

        }
    }
}
