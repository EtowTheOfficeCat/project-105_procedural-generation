﻿using UnityEngine;

public class CircleArcConstructor : MonoBehaviour
{
    // Algorithm source :
    // https://math.stackexchange.com/questions/1781438/finding-the-center-of-a-circle-given-two-points-and-a-radius-algebraically

    [SerializeField] private bool doVisualize;
    [SerializeField] private bool SwitchifTrue = false;
    [SerializeField] private float radiusPlus = 1;
    [SerializeField] private int noOfSegments =6;
    [SerializeField] Transform p1Transf;
    [SerializeField] Transform p2Transf;
    [SerializeField] float height;
    [SerializeField] private AnimationCurve heightCurve;
    private Vector3 p1;
    private Vector3 p2;
    private Vector3 center;
    private float angle;
    private float epsilon = 0.00001f;

   

    private void ConstructArc()
    {
        Vector3 point1 = p1 = p1Transf.position;
        Vector3 point2 = p2 = p2Transf.position;
        p1 = new Vector3(p1.x, 0f, p1.z);
        p2 = new Vector3(p2.x, 0f, p2.z);
        Debug.DrawLine(p1, p2, Color.yellow);
        float xa = (p2.x - p1.x) * 0.5f;
        float za = (p2.z - p1.z) * 0.5f;
        Vector3 a = new Vector3(xa, 0f, za);
        Vector3 middle = p1 + a;
        Debug.DrawLine(p1, middle, Color.red);
        float aMagnitude = a.magnitude;
        float radius = aMagnitude + radiusPlus + epsilon;
        float bMagnitude = Mathf.Sqrt(radius * radius - a.sqrMagnitude);
        float x3;
        float y3;

        if (SwitchifTrue)
        {
             x3 = middle.x + (bMagnitude * za) / aMagnitude;
             y3 = middle.z - (bMagnitude * xa) / aMagnitude;
        }
        else
        {
            x3 = middle.x + (bMagnitude * za) / aMagnitude;
            y3 = middle.z - (bMagnitude * xa) / aMagnitude;
        }

        center = new Vector3(x3, 0f, y3);
        Debug.DrawLine(p1, center, Color.cyan);
        Debug.DrawLine(p2, center, Color.cyan);
        Vector3 p1ToCenter = center - p1;
        Vector3 p2toCenter = center - p2;
        Vector3 centerToP1 = -p1ToCenter;

        angle = Vector3.Angle(p1ToCenter, p2toCenter);
        if (SwitchifTrue)
        {
            angle = -angle;
        }
        float step = angle / noOfSegments;
        for (int i = 0; i < noOfSegments + 1; i++)

        {
            Quaternion rot = Quaternion.identity;
            if (i != 0)
            {
                rot = Quaternion.Euler(new Vector3(0f, step * i, 0f));
            }

            Vector3 dir = rot * centerToP1;
            float t = i / (float)noOfSegments;
            t = heightCurve.Evaluate(t);
            float y = Mathf.Lerp(point1.y, point2.y, t);
            dir = new Vector3(dir.x, y, dir.z);

            Debug.DrawRay(center, dir, Color.green);
        }
    }

    private void OnDrawGizmos()
    {
        if (!doVisualize)
        {
            return;
        }
        ConstructArc();
    }
}
